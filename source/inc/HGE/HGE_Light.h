#ifndef HGE_LIGHT_H
#define HGE_LIGHT_H

#include "HGE_ECS.h"
#include "HGE_Math3D.h"

#define HGE_MAX_LIGHTS 10

typedef struct {
    int id;
    hge_vec3 ambient;
    hge_vec3 diffuse;
    hge_vec3 specular;

    float constant;
    float linear;
    float quadratic;
} hge_light;

hge_light hgeLight(
    hge_vec3 ambient,
    hge_vec3 diffuse,
    hge_vec3 specular,
    float constant,
    float linear,
    float quadratic
  );

void hgeLightDataReset();

void hge_system_light(hge_entity* entity, hge_vec3* position, hge_light* light);

#endif
