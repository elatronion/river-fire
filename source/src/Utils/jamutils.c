#include "jamutils.h"
#include <stdio.h>
#include <string.h>

float distance(hge_vec3 position_a, hge_vec3 position_b) {
  return sqrt(pow((position_b.x - position_a.x), 2) + pow((position_b.y - position_a.y), 2) + pow((position_b.z - position_a.z), 2));
}

float RandomFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

// 100, 200
// 0.5
// 150

float easeinout(float a, float b, float t) {
  float diff = b - a;
  return (pow(t, 2)/(pow(t, 2) + pow(1 - t, 2))) * diff + a;
}

void binwrite(const char* file_name, pointer data, size_t size) {
  FILE * fp = fopen(file_name, "wb");

   if (fp == NULL)  /* If an error occurs during the file creation */
   {
     fprintf(stderr, "fopen() failed for '%s'\n", file_name);
   }
   else
   {
     size_t element_size = size;
     size_t elements_to_write = 1;

   size_t elements_written = fwrite(data, element_size, elements_to_write, fp);
     if (elements_written != elements_to_write)
     {
       fprintf(stderr, "fwrite() failed: wrote only %zu out of %zu elements.\n",
         elements_written, elements_to_write);
     }

     fclose(fp);
   }
}

void binread(const char* file_name, pointer data, size_t size) {
	FILE * fp = fopen(file_name, "rb");

   if (fp == NULL)
   {
     fprintf(stderr, "fopen() failed for '%s'\n", file_name);
   }
   else
   {
		   fread(data, size, 1, fp);
       fclose(fp);
   }
}

jam_time float_to_int_time(float time) {
  jam_time jt = { 0, 0, 0, 0 };
  jt.miliseconds = (int)(time*1000)%1000;
  jt.seconds = (int)(time)%60;
  jt.minutes = (int)(time/60)%60;
  jt.hours = (int)(time/3600);
  return jt;
}
