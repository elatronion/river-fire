#ifndef PROJECTILE_H
#define PROJECTILE_H
#include <HGE/HGE_Core.h>

typedef struct {

} projectile_component;

hge_entity* create_projectile(hge_mesh mesh, hge_vec3 position, hge_vec3 velocity);

void system_projectile(hge_entity* entity, hge_vec3* position, hge_vec3* velocity, projectile_component* projectile);

#endif
