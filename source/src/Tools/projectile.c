#include "projectile.h"
#include "level.h"

hge_entity* create_projectile(hge_mesh mesh, hge_vec3 position, hge_vec3 velocity) {
  hge_entity* projectile_entity = hgeCreateEntity();
  projectile_component projectile;
  hgeAddComponent(projectile_entity, hgeCreateComponent("position", &position, sizeof(position)));
  hgeAddComponent(projectile_entity, hgeCreateComponent("velocity", &velocity, sizeof(velocity)));
  hgeAddComponent(projectile_entity, hgeCreateComponent("projectile", &projectile, sizeof(projectile)));

  c_mesh cmesh;
  cmesh.mesh = mesh;
	cmesh.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(projectile_entity, hgeCreateComponent("mesh", &cmesh, sizeof(cmesh)));
  add_to_level_entities(projectile_entity);
  return projectile_entity;
}

void system_projectile(hge_entity* entity, hge_vec3* position, hge_vec3* velocity, projectile_component* projectile) {
  position->x += velocity->x * hgeDeltaTime();
  position->y += velocity->y * hgeDeltaTime();
  position->z += velocity->z * hgeDeltaTime();

  velocity->y -= 15 * hgeDeltaTime();

  if(position->y <= 0) {
    if(isPositionOverGround(*position)) {
          int x = (int)(ceil(floor(position->x-1)/(TILESIZE)));
        	int y = (int)(ceil(floor(position->z-1)/(TILESIZE)));
          level_modify_ground_state(x, y, TILE_STATE_FIRE);
          hge_entity* fire_mesh = create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("fire"));
          particle_system ps;
          ps.num_particles = 0;
          ps.particle_mesh = hgeResourcesQueryMesh("particle");
          ps.particle_mesh.material.diffuse = hgeResourcesQueryTexture("image");
          hgeAddComponent(fire_mesh, hgeCreateComponent("particle system", &ps, sizeof(ps)));
    }
    hgeDestroyEntity(entity);
  }
}
