#include "level.h"
#include "tmx.h"
#include "camera_fx.h"

hge_entity* level_entities[10000];
int num_level_entities = 0;

int num_of_ground_burned = 0;
int num_of_ground_total = 256;
int num_of_unchanged_steps = 0;

int ground_state[16][16];

char last_loaded_level[255];

bool loading = false;

int count_num_of_ground() {
	int count = 0;
	for(int y = 0; y < 16; y++)
	for(int x = 0; x < 16; x++) {
		if(ground_state[x][y] == TILE_STATE_GROUND) count++;
	}
	return count;
}

void add_to_level_entities(hge_entity* entity) {
	level_entities[num_level_entities] = entity;
	num_level_entities++;
}

hge_entity* create_explosive_entity(hge_vec3 position, hge_mesh mesh) {
	hge_entity* mesh_entity = hgeCreateEntity();
	c_mesh cmesh;
	cmesh.mesh = mesh;
	cmesh.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(mesh_entity, hgeCreateComponent("mesh", &cmesh, sizeof(cmesh)));
	hgeAddComponent(mesh_entity, hgeCreateComponent("position", &position, sizeof(position)));

	explosive_component explosion;
	hgeAddComponent(mesh_entity, hgeCreateComponent("explosive", &explosion, sizeof(explosion)));

	add_to_level_entities(mesh_entity);
	return mesh_entity;
}

hge_entity* create_mesh_entity(hge_vec3 position, hge_mesh mesh) {
	hge_entity* mesh_entity = hgeCreateEntity();
	c_mesh cmesh;
	cmesh.mesh = mesh;
	cmesh.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(mesh_entity, hgeCreateComponent("mesh", &cmesh, sizeof(cmesh)));
	hgeAddComponent(mesh_entity, hgeCreateComponent("position", &position, sizeof(position)));

	add_to_level_entities(mesh_entity);
	return mesh_entity;
}

void create_firepit(hge_vec3 position) {
	hge_mesh firepit_rock = hgeResourcesQueryMesh("firepit rocks");
	hge_mesh firepit_logs = hgeResourcesQueryMesh("firepit logs");

	hge_entity* mesh_entity = hgeCreateEntity();
	c_mesh cmesh;
	cmesh.mesh = firepit_rock;
	cmesh.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(mesh_entity, hgeCreateComponent("mesh", &cmesh, sizeof(cmesh)));
	particle_system ps;
	ps.num_particles = 0;
	ps.particle_mesh = hgeResourcesQueryMesh("particle");
	ps.particle_mesh.material.diffuse = hgeResourcesQueryTexture("image");
	hgeAddComponent(mesh_entity, hgeCreateComponent("particle system", &ps, sizeof(ps)));
	hgeAddComponent(mesh_entity, hgeCreateComponent("position", &position, sizeof(position)));

	hge_entity* mesh_entity2 = hgeCreateEntity();
	c_mesh cmesh2;
	cmesh2.mesh = firepit_logs;
	cmesh2.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh2.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(mesh_entity2, hgeCreateComponent("mesh", &cmesh2, sizeof(cmesh2)));
	hgeAddComponent(mesh_entity2, hgeCreateComponent("position", &position, sizeof(position)));

	add_to_level_entities(mesh_entity);
	add_to_level_entities(mesh_entity2);

  create_mesh_entity(position, hgeResourcesQueryMesh("tile flat"));
}

void create_grass(hge_vec3 position) {
  hge_entity* mesh_entity = hgeCreateEntity();
	c_mesh cmesh;
	cmesh.mesh = hgeResourcesQueryMesh("grass");;
	cmesh.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(mesh_entity, hgeCreateComponent("mesh", &cmesh, sizeof(cmesh)));
	hgeAddComponent(mesh_entity, hgeCreateComponent("position", &position, sizeof(position)));

	add_to_level_entities(mesh_entity);

  create_mesh_entity(position, hgeResourcesQueryMesh("tile flat"));
}

void create_bush(hge_vec3 position) {
  hge_entity* mesh_entity = hgeCreateEntity();
	c_mesh cmesh;
	cmesh.mesh = hgeResourcesQueryMesh("bush");;
	cmesh.mesh.material.diffuse = hgeResourcesQueryTexture("image");
	cmesh.mesh.material.normal = hgeResourcesQueryTexture("DEFAULT NORMAL");
	hgeAddComponent(mesh_entity, hgeCreateComponent("mesh", &cmesh, sizeof(cmesh)));
	hgeAddComponent(mesh_entity, hgeCreateComponent("position", &position, sizeof(position)));

	add_to_level_entities(mesh_entity);

  create_mesh_entity(position, hgeResourcesQueryMesh("tile flat"));
}

void create_player(hge_vec3 position, hge_vec3 orientation) {
	// Camera
	hge_entity* camera_entity = hgeCreateEntity();
	//hge_camera cam = {true, true, 1.f/6.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), -100.0f, 100.0f };
	hge_camera cam = {true, false, 90, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), 0.5f, 105.0f };
	hge_vec3 camera_position = position;
	orientation_component camera_orientation = { orientation.x, orientation.y, orientation.z };
	hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
	tag_component activecam_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
	camera_fx cfx = { hgeVec3(0, 0, 0), hgeVec3(0, 0, 0), 0.0f };
	hgeAddComponent(camera_entity, hgeCreateComponent("camera fx", &cfx, sizeof(cfx)));

	player_controller controller;
	controller.target_position = camera_position;
	controller.speed = 5.f;
	controller.anim_timer = 0.0f;
	controller.anim_state = 0;
	controller.score = 0.0f;
	hgeAddComponent(camera_entity, hgeCreateComponent("player controller", &controller, sizeof(controller)));
	add_to_level_entities(camera_entity);
}

void create_level_selector() {
	// Camera
	hge_entity* camera_entity = hgeCreateEntity();
	//hge_camera cam = {true, true, 1.f/6.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), -100.0f, 100.0f };
	hge_camera cam = {true, false, 90, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), 0.5f, 105.0f };
	hge_vec3 camera_position = { 0, 0, 0 };
	orientation_component camera_orientation = { 0, 0, 0 };
	hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
	tag_component activecam_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
	camera_fx cfx = { hgeVec3(0, 0, 0), hgeVec3(0, 0, 0), 0.0f };
	hgeAddComponent(camera_entity, hgeCreateComponent("camera fx", &cfx, sizeof(cfx)));

	level_selector selector = { 0, 0.0f, 0, 0 };
	hgeAddComponent(camera_entity, hgeCreateComponent("level selector", &selector, sizeof(selector)));
	add_to_level_entities(camera_entity);
}

// TMX //

void ParseTMXData(tmx_map* map) {
  printf("Map Size: %dx%d\n", map->width, map->height);

  tmx_layer* layer = map->ly_head;
  while(layer != NULL) {
    if(strcmp(layer->name, "floor") == 0) {
      for(int i = 0; i < map->width*map->height; i++) {
        int x = i%map->width;
        int y = i/map->width;
        switch (layer->content.gids[i]) {
						case 0:
							ground_state[(int)x][(int)y] = TILE_STATE_INVINCIBLE;
							create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("water"));
						break;
            case 1:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
            case 2:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile topleft"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt topleft"));
							create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("water"));
            break;
            case 3:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile topright"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt topright"));
							create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("water"));
            break;
            case 4:
              create_grass(hgeVec3(x*TILESIZE, 0, y*TILESIZE));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
            case 5:
              create_bush(hgeVec3(x*TILESIZE, 0, y*TILESIZE));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
            case 6:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("pumkin"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
            case 7:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tree"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
						case 8:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tent"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
						case 9:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("table"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
            case 241:
							//ground_state[(int)x][(int)y] = TILE_STATE_FIRE;
              create_firepit(hgeVec3(x*TILESIZE, 0, y*TILESIZE));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
            break;
            case 242:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile bottomleft"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt bottomleft"));
							create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("water"));
            break;
            case 243:
              create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile bottomright"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt bottomright"));
							create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("water"));
            break;
						case 244:
						{
							hge_entity* bridge_entity = create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile bridge"));
							bridge_component bridge_c;
							hgeAddComponent(bridge_entity, hgeCreateComponent("bridge", &bridge_c, sizeof(bridge_c)));
						}
							create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("water"));
						break;
						case 245:
							create_explosive_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("propane"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
						break;
						case 481:
							create_player(hgeVec3(x*TILESIZE, 1.25f, y*TILESIZE), hgeVec3(0, 90.0f, 0));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
						break;
						case 482:
							create_level_selector();
							create_mesh_entity(hgeVec3(x*TILESIZE, 0, y*TILESIZE), hgeResourcesQueryMesh("tile flat"));
							create_mesh_entity(hgeVec3(x*TILESIZE, 0.0f, y*TILESIZE), hgeResourcesQueryMesh("dirt"));
						break;
        }
      }
    }
    layer = layer->next;
  }
}

void ReloadLevel() {
	LoadLevel(last_loaded_level);
}

void load_tmx_level(const char* level_path) {
	hgeUseShader((hgeResourcesQueryShader("framebuffer_shader")));
	hgeShaderSetBool(hgeResourcesQueryShader("framebuffer_shader"), "red_splash", false);
	hgeShaderSetBool(hgeResourcesQueryShader("framebuffer_shader"), "blue_splash", false);
	set_gamestate(GAME_PLAY);
	UnloadLevel();
	create_mesh_entity(hgeVec3(8*TILESIZE, 0.0f, 8*TILESIZE), hgeResourcesQueryMesh("sphere map"));
  //strcpy(last_loaded_scene, level_path);

  tmx_map *map = tmx_load(level_path);
  if (!map) {
		tmx_perror("Cannot load map");
    //LoadLevel("res/levels/error.tmx");
		return;
	}
  ParseTMXData(map);
  tmx_map_free(map);

	num_of_ground_total = count_num_of_ground();
}

void LoadLevel(const char* level_path) {
	printf("Loading Level '%s'\n", level_path);
	loading = true;
	strcpy(last_loaded_level, level_path);
}

void UnloadLevel() {
	num_of_ground_total = 256;
	num_of_ground_burned = 0;
	num_of_unchanged_steps = 0;

	for(int y = 0; y < 16; y++)
	for(int x = 0; x < 16; x++) {
		ground_state[x][y] = TILE_STATE_GROUND;
	}

	for(int i = 0; i < num_level_entities; i++) {
		hgeDestroyEntity(level_entities[i]);
	}
	num_level_entities = 0;
}

char* cur_level() {
	return last_loaded_level;
}

void level_modify_ground_state(int x, int y, int state) {
	ground_state[x][y] = state;
}

void StepLevel() {
	int tmp_ground_state[16][16];
	memcpy(tmp_ground_state, ground_state, sizeof(ground_state));

	//ground_state[x-1][y];
	//ground_state[x+1][y];
	//ground_state[x][y+1];
	//ground_state[x][y-1];

	for(int y = 0; y < 16; y++)
	for(int x = 0; x < 16; x++) {
		int num_fire_around = 0;
		switch(ground_state[x][y]) {
			case TILE_STATE_GROUND:
				if(x > 0) {
					if(ground_state[x-1][y] == TILE_STATE_FIRE) num_fire_around++;
				}
				if(x < 15) {
					if(ground_state[x+1][y] == TILE_STATE_FIRE) num_fire_around++;
				}
				if(y > 0) {
					if(ground_state[x][y-1] == TILE_STATE_FIRE) num_fire_around++;
				}
				if(y < 15) {
					if(ground_state[x][y+1] == TILE_STATE_FIRE) num_fire_around++;
				}

				if(num_fire_around > 0) {
					tmp_ground_state[x][y] = TILE_STATE_FIRE;
					hge_entity* fire_mesh = create_mesh_entity(hgeVec3(x*TILESIZE, -0.5f, y*TILESIZE), hgeResourcesQueryMesh("fire"));
					particle_system ps;
					ps.num_particles = 0;
					ps.particle_mesh = hgeResourcesQueryMesh("particle");
					ps.particle_mesh.material.diffuse = hgeResourcesQueryTexture("image");
					hgeAddComponent(fire_mesh, hgeCreateComponent("particle system", &ps, sizeof(ps)));
				}
			break;
		}
	}

	memcpy(ground_state, tmp_ground_state, sizeof(ground_state));

	if(get_gamestate() == GAME_PLAY) {
		float new_num_of_ground_burned = num_of_ground_total - count_num_of_ground();
		if(num_of_ground_burned != 0 && num_of_ground_burned == new_num_of_ground_burned) {
			num_of_unchanged_steps++;
			if(num_of_unchanged_steps >= 3) {
				set_gamestate(GAME_WIN);
			}
		}
		num_of_ground_burned = new_num_of_ground_burned;
	}
}

void system_level_manager(hge_entity* entity, level_manager* level) {
	float letterbox_speed = 2;
	if(loading) {
		level->level_loading_timer += letterbox_speed * hgeDeltaTime();
		if(level->level_loading_timer >= 1.15f) {
			loading = false;
			load_tmx_level(last_loaded_level);
		}
	} else if(level->level_loading_timer > 0.f){
		level->level_loading_timer -= letterbox_speed * hgeDeltaTime();
	} else {
		level->level_loading_timer = 0.f;
	}
	hgeUseShader(hgeResourcesQueryShader("framebuffer_shader"));
	hgeShaderSetFloat(hgeResourcesQueryShader("framebuffer_shader"), "letterbox_percentage", fmin(1.0f, level->level_loading_timer));
	hgeUseShader(hgeResourcesQueryShader("sprite_shader"));

	level->step_timer += hgeDeltaTime();
	if(level->step_timer >= 1/level->frequency) {
		StepLevel();
		level->step_timer = 0;
	}
}

bool isPositionInLevel(hge_vec3 position) {
	int x = (int)(ceil(floor(position.x-1)/(TILESIZE)));
	int y = (int)(ceil(floor(position.z-1)/(TILESIZE)));
	return (x >= 0 && x < 16 &&
					y >= 0 && y < 16);
}

bool isPositionOverGround(hge_vec3 position) {
	if(!isPositionInLevel(position)) return TILE_STATE_INVINCIBLE;
	int x = (int)(ceil(floor(position.x-1)/(TILESIZE)));
	int y = (int)(ceil(floor(position.z-1)/(TILESIZE)));
	return (ground_state[x][y] == TILE_STATE_GROUND);
}

int tileAtPosition(hge_vec3 position) {
	if(!isPositionInLevel(position)) return TILE_STATE_INVINCIBLE;
	int x = (int)(ceil(floor(position.x-1)/(TILESIZE)));
	int y = (int)(ceil(floor(position.z-1)/(TILESIZE)));
	return ground_state[x][y];
}

float level_precentage() {
	num_of_ground_burned = num_of_ground_total - count_num_of_ground();
	return ((float)num_of_ground_burned/(float)num_of_ground_total) * 100.f;
	//printf("%f% : %d/%d Burned\n", percentage_burned, num_of_ground_burned, num_of_ground_total);
}
