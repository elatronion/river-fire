#include "particles.h"
#include "jamutils.h"

void add_particle(particle_system* ps, hge_vec3 position) {
  if(ps->num_particles >= MAX_PARTICLES) return;
  particle p = {
    position,
    hgeVec3(RandomFloat(-0.25, 0.25), 1, RandomFloat(-0.25, 0.25)),
    7.f };
  ps->particles[ps->num_particles] = p;
  ps->num_particles++;
}

void remove_particle(particle_system* ps, int index) {
  for(int i = index; i < ps->num_particles-1; ++i) {
    ps->particles[i] = ps->particles[i + 1];
  }
  // delete entity from array
//  for (int i = index; i < active_world.NUM_ENTITIES-1; ++i)
//    active_world.entities[i] = active_world.entities[i + 1]; // copy next element left
  ps->num_particles--;
}

void update_particle(particle_system* ps, particle* p, int index) {
  p->position.x += p->velocity.x * hgeDeltaTime();
  p->position.y += p->velocity.y * hgeDeltaTime();
  p->position.z += p->velocity.z * hgeDeltaTime();
  p->lifetime -= hgeDeltaTime();
  if(p->lifetime <= 0) {
    remove_particle(ps, index);
  }
}

void draw_particle(particle_system* ps, particle* p) {
  float scale = 3 * (p->lifetime/3.f);
  //float scale =1;
  hgeRenderMesh(hgeResourcesQueryShader("sprite_shader"), ps->particle_mesh, p->position, hgeVec3(scale, scale, scale));
}

float test_timer = 0.0f;

void system_particles(hge_entity* entity, hge_vec3* position, particle_system* ps) {
  test_timer += hgeDeltaTime();
  if(test_timer >= 0.75f) {
    add_particle(ps, hgeVec3(position->x, position->y-0.25f, position->z));
    test_timer = 0;
  }
  hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "fire", false);
  hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "water", false);
  for(int i = 0; i < ps->num_particles; i++) {
    update_particle(ps, &ps->particles[i], i);
    draw_particle(ps, &ps->particles[i]);
  }

}
