#include "gamestate.h"
#include <HGE/HGE_Core.h>
#include "level.h"
#include "level_selector.h"

state gamestate = GAME_PLAY;

void process_state_change(state state) {
  switch(state) {
    case GAME_PLAY:
    break;
    case GAME_OVER:
    {
      player_controller* controller;
      hge_ecs_request request = hgeECSRequest(1, "player controller");
      for(int i = 0; i < request.NUM_ENTITIES; i++) {
        hge_entity* player_entity = request.entities[i];
        controller = (player_controller*) player_entity->components[hgeQuery(player_entity, "player controller")].data;
      }
      if(controller) {
        if(controller->score >= 100) {
          char title[50];
          sscanf(cur_level(), "res/levels/%[^\n].tmx", &title);
          title[strlen(title)-4] = '\0';
          char path[255];
          sprintf(path, "%s%s%s", "res/levels/scores/", title, ".bin");

          level_data lvl_data = { 0.0f, 0.0f };
          binread(path, &lvl_data, sizeof(lvl_data));

          if(controller->score > lvl_data.score) {
            lvl_data.score = controller->score;
            lvl_data.time = controller->time;
            binwrite(path, &lvl_data, sizeof(lvl_data));
            clean_loaded_level_files();
            load_level_folder();
          } else if(controller->score == lvl_data.score && controller->time < lvl_data.time) {
            lvl_data.score = controller->score;
            lvl_data.time = controller->time;
            binwrite(path, &lvl_data, sizeof(lvl_data));
            clean_loaded_level_files();
            load_level_folder();
          }

        }
      }
    }
    break;
    case GAME_WIN:
      {
        player_controller* controller;
        hge_ecs_request request = hgeECSRequest(1, "player controller");
        for(int i = 0; i < request.NUM_ENTITIES; i++) {
          hge_entity* player_entity = request.entities[i];
          controller = (player_controller*) player_entity->components[hgeQuery(player_entity, "player controller")].data;
        }

        if(controller) {
          char title[50];
          sscanf(cur_level(), "res/levels/%[^\n].tmx", &title);
          title[strlen(title)-4] = '\0';
          char path[255];
          sprintf(path, "%s%s%s", "res/levels/scores/", title, ".bin");

          level_data lvl_data = { 0.0f, 0.0f };
          binread(path, &lvl_data, sizeof(lvl_data));

          if(controller->score > lvl_data.score) {
            lvl_data.score = controller->score;
            lvl_data.time = controller->time;
            binwrite(path, &lvl_data, sizeof(lvl_data));
            clean_loaded_level_files();
            load_level_folder();
          } else if(controller->score == lvl_data.score && controller->time < lvl_data.time) {
            lvl_data.score = controller->score;
            lvl_data.time = controller->time;
            binwrite(path, &lvl_data, sizeof(lvl_data));
            clean_loaded_level_files();
            load_level_folder();
          }
        }
      }
    break;
  }
}

state get_gamestate() {
  return gamestate;
}

void set_gamestate(state s) {
  gamestate = s;
  process_state_change(gamestate);
}
