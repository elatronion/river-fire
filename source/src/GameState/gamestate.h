#ifndef GAMESTATE_H
#define GAMESTATE_H

typedef enum {
  GAME_PLAY, GAME_OVER, GAME_WIN
} state;

state get_gamestate();
void set_gamestate(state s);

#endif
