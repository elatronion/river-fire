#include <HGE/HGE_Core.h>
#include <stdio.h>
#include <math.h>

#include "level.h"
#include "projectile.h"
#include "explosive.h"
#include "camera_fx.h"
#include "gamestate.h"
#include "text.h"
#include "jamutils.h"

void mesh_rendering_system(hge_entity* entity, hge_vec3* position, c_mesh* cmesh) {
	if (cmesh->mesh.VAO == hgeResourcesQueryMesh("fire").VAO) {
		hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "fire", true);
	} else if (cmesh->mesh.VAO == hgeResourcesQueryMesh("water").VAO || cmesh->mesh.VAO == hgeResourcesQueryMesh("sphere map").VAO) {
		hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "water", true);
	} else {
		hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "fire", false);
		hgeShaderSetBool(hgeResourcesQueryShader("sprite_shader"), "water", false);
	}
	hgeRenderMesh(hgeResourcesQueryShader("sprite_shader"), cmesh->mesh, *position, hgeVec3(1, 1, 1));
}

/* FreeCam
void camera_controller(hge_entity* entity, c_camera_controller* controller, hge_camera* camera, hge_vec3* position, orientation_component* orientation) {
	//hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "viewPos", *camera_position);

	float speed = 5.0f;
	//float frictionCoeff = 4.0f;

	float yaw_speed = 100.0f;
	if(hgeInputGetKey(HGE_KEY_SPACE)) {
		controller->target_position.y += speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_LEFT_CONTROL)) {
		controller->target_position.y -= speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_A)) {
		controller->target_position.x -= cos(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
		controller->target_position.z -= sin(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_D)) {
		controller->target_position.x += cos(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
		controller->target_position.z += sin(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
	}

	if(hgeInputGetKey(HGE_KEY_W)) {
		controller->target_position.x += cos(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
		controller->target_position.z += sin(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_S)) {
		controller->target_position.x -= cos(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
		controller->target_position.z -= sin(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
	}

	if(hgeInputGetKey(HGE_KEY_LEFT)) {
		orientation->yaw -= yaw_speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_RIGHT)) {
		orientation->yaw += yaw_speed * hgeDeltaTime();
	}

	position->x += (controller->target_position.x - position->x) * controller->speed * hgeDeltaTime();
	position->y += (controller->target_position.y - position->y) * controller->speed * hgeDeltaTime();
	position->z += (controller->target_position.z - position->z) * controller->speed * hgeDeltaTime();
}
*/

void system_player_controller(hge_entity* entity, player_controller* controller, hge_vec3* position, orientation_component* orientation) {
	//hgeShaderSetVec3(hgeResourcesQueryShader("sprite_shader"), "viewPos", *camera_position);
	hgeShaderSetFloat(hgeResourcesQueryShader("sprite_shader"), "runtime", hgeRuntime());

	char lvl_percent_text[255];
	sprintf(lvl_percent_text, "%.2f%s", controller->score, "%");
	//jam_time jt = float_to_int_time(controller->time);
	//sprintf(lvl_percent_text, "%02d:%02d.%03d", jt.minutes, jt.seconds, jt.miliseconds);
	float ammount = 0.0f;
	float opacity = 0.25f;
	float x = 0;
	float y = 0;
	switch(get_gamestate()) {
		case GAME_PLAY:
		controller->score = level_precentage();
		jamTextRenderSimple(lvl_percent_text, true, hgeVec3(0, hgeWindowHeight()/2 - 50, 0), 1.0f, hgeVec4(1, 1, 1, 0.25f));
		break;
		case GAME_OVER:
		case GAME_WIN:
		controller->anim_timer += hgeDeltaTime();
		switch(controller->anim_state) {
			case 0:
				ammount = easeinout(hgeWindowHeight()/2 - 50.f, 0, fmin(1.0f, controller->anim_timer/.75f));
				opacity = easeinout(0.25f, 1, fmin(1.0f, controller->anim_timer/.75f));
				for(int i = 0; i < 3; i++) {
					jamTextRenderSimple(lvl_percent_text, true, hgeVec3(RandomFloat(-4, 4), ammount + RandomFloat(-4, 4), (99-3)+i), 1.0f, hgeVec4(0, 0, 0, opacity));
				}
				x = RandomFloat(-2, 2);
				y = RandomFloat(-2, 2);
				jamTextRenderSimple(lvl_percent_text, true, hgeVec3(x, ammount + y, 100), 1.0f, hgeVec4(1, 1, 1, opacity));

				if(controller->score >= 100) {
					if(controller->anim_timer >= 1.5f) {
						for(int i = 0; i < 3; i++) {
							jamTextRenderSimple("Total Pyromaniac", true, hgeVec3(RandomFloat(-4, 4), RandomFloat(-4, 4) + 200, (99-3)+i), 1.0f, hgeVec4(0, 0, 0, 1));
						}
						jamTextRenderSimple("Total Pyromaniac", true, hgeVec3(RandomFloat(-2, 2), RandomFloat(-2, 2) + 200, 100), 1.0f, hgeVec4(1, 1, 1, 1));
					}
				}

				if(controller->anim_timer >= 10.f) {
					for(int i = 0; i < 3; i++) {
						jamTextRenderSimple("Press Enter To Continue", true, hgeVec3(RandomFloat(-4, 4), RandomFloat(-4, 4) - 200, (99-3)+i), 1.0f, hgeVec4(0, 0, 0, 1));
					}
					jamTextRenderSimple("Press Enter To Continue", true, hgeVec3(RandomFloat(-2, 2), RandomFloat(-2, 2) - 200, 100), 1.0f, hgeVec4(1, 1, 1, 1));
				}

				/*if(controller->anim_timer >= .75f) {
					controller->anim_state = 1;
					controller->anim_timer = 0;
				}
			break;
			case 1:
				ammount = easeinout(1, 1.5f, fmin(1.0f, controller->anim_timer/.75f));
				jamTextRenderSimple(lvl_percent_text, true, hgeVec3(0, 0, 0), ammount, hgeVec4(1, 1, 1, 1));
			break;*/
		}
		break;
	}

	float player_height = 1.25f;
	position->x += (controller->target_position.x - position->x) * controller->speed * hgeDeltaTime();
	position->y += ((player_height + controller->target_position.y) - position->y) * controller->speed * hgeDeltaTime();
	position->z += (controller->target_position.z - position->z) * controller->speed * hgeDeltaTime();

	if(get_gamestate() == GAME_OVER) {
		if(hgeInputGetKeyDown(HGE_KEY_ENTER)) {
			if(controller->score >= 100)
				LoadLevel("res/levels/level selection.tmx");
			else
				ReloadLevel();
		} else if(hgeInputGetKeyDown(HGE_KEY_ESCAPE)) {
			if(controller->score >= 100)
				ReloadLevel();
			else
				LoadLevel("res/levels/level selection.tmx");
		}
		if(tileAtPosition(*position) == TILE_STATE_INVINCIBLE) {
			hgeUseShader((hgeResourcesQueryShader("framebuffer_shader")));
			hgeShaderSetBool(hgeResourcesQueryShader("framebuffer_shader"), "blue_splash", true);
			hgeUseShader((hgeResourcesQueryShader("sprite_shader")));
			controller->target_position.y = -player_height -1.25f;
		} else {
			hgeUseShader((hgeResourcesQueryShader("framebuffer_shader")));
			hgeShaderSetBool(hgeResourcesQueryShader("framebuffer_shader"), "red_splash", true);
			hgeUseShader((hgeResourcesQueryShader("sprite_shader")));
			controller->target_position.y = -player_height + 0.75f;
		}
	} else if(get_gamestate() == GAME_WIN) {
		if(hgeInputGetKeyDown(HGE_KEY_ENTER)) {
			LoadLevel("res/levels/level selection.tmx");
		} else if(hgeInputGetKeyDown(HGE_KEY_ESCAPE)) {
			ReloadLevel();
		}
	} else {
		hgeUseShader((hgeResourcesQueryShader("framebuffer_shader")));
		hgeShaderSetBool(hgeResourcesQueryShader("framebuffer_shader"), "red_splash", false);
		hgeShaderSetBool(hgeResourcesQueryShader("framebuffer_shader"), "blue_splash", false);
		hgeUseShader((hgeResourcesQueryShader("sprite_shader")));
	}

	if(get_gamestate() != GAME_PLAY) return;
	controller->time += hgeDeltaTime();

	if(hgeInputGetKeyDown(HGE_KEY_E)) {
		float throw_force = 20;
		create_projectile(hgeResourcesQueryMesh("molotov"), *position, hgeVec3(cos(hgeMathRadians(orientation->yaw)) * throw_force, sin(hgeMathRadians(orientation->pitch)) * throw_force, sin(hgeMathRadians(orientation->yaw)) * throw_force));
	}

	if(hgeInputGetKeyDown(HGE_KEY_ENTER)) {
		float closest_bridge_dist = 100;
		hge_vec3* closest_bridge_position = NULL;
		hge_entity* closest_bridge_entity = NULL;
		hge_ecs_request bridge_request = hgeECSRequest(1, "bridge");
		for(int i = 0; i < bridge_request.NUM_ENTITIES; i++) {
			hge_entity* bridge_entity = bridge_request.entities[i];
		  hge_vec3* bridge_position = (hge_vec3*) bridge_entity->components[hgeQuery(bridge_entity, "position")].data;
			if(closest_bridge_dist >= distance(*position, *bridge_position) && distance(*position, *bridge_position) <= 4) {
				closest_bridge_dist = distance(*position, *bridge_position);
				closest_bridge_position = bridge_position;
				closest_bridge_entity = bridge_entity;
			}
		}
		if(closest_bridge_entity) {
			int x = (int)(ceil(floor(closest_bridge_position->x-1)/(TILESIZE)));
			int y = (int)(ceil(floor(closest_bridge_position->z-1)/(TILESIZE)));
			level_modify_ground_state(x, y, TILE_STATE_INVINCIBLE);
			hgeDestroyEntity(closest_bridge_entity);
		}
	}

	float speed = 2.5f;

	float yaw_speed = 100.0f;

	bool FORWARD = hgeInputGetKey(HGE_KEY_W);
	bool BACKWARD = hgeInputGetKey(HGE_KEY_S);
	bool STRAFE_RIGHT = hgeInputGetKey(HGE_KEY_D);
	bool STRAFE_LEFT = hgeInputGetKey(HGE_KEY_A);

	if(FORWARD) {
		controller->target_position.x += cos(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
		controller->target_position.z += sin(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
	}
	if(BACKWARD) {
		controller->target_position.x -= cos(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
		controller->target_position.z -= sin(hgeMathRadians(orientation->yaw)) * speed * hgeDeltaTime();
	}
	if(STRAFE_RIGHT) {
		controller->target_position.x += cos(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
		controller->target_position.z += sin(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
	}
	if(STRAFE_LEFT) {
		controller->target_position.x -= cos(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
		controller->target_position.z -= sin(hgeMathRadians(orientation->yaw+90)) * speed * hgeDeltaTime();
	}

	if(FORWARD || BACKWARD || STRAFE_LEFT || STRAFE_RIGHT) {
		controller->target_position.y = (cos(position->x * 4) + sin(position->z * 4))*0.09f;
	} else {
		controller->target_position.y = 0;
	}

	if(hgeInputGetKey(HGE_KEY_LEFT)) {
		orientation->yaw -= yaw_speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_RIGHT)) {
		orientation->yaw += yaw_speed * hgeDeltaTime();
	}
	if(hgeInputGetKey(HGE_KEY_UP)) {
		orientation->pitch += yaw_speed * hgeDeltaTime();
		if(orientation->pitch > 45) orientation->pitch = 45;
	}
	if(hgeInputGetKey(HGE_KEY_DOWN)) {
		orientation->pitch -= yaw_speed * hgeDeltaTime();
		if(orientation->pitch < -45) orientation->pitch = -45;
	}

	if(!isPositionOverGround(*position)) {
		set_gamestate(GAME_OVER);
	}
	/*if(!isPositionInLevel(*position)) {
			controller->target_position.y = 10;
	}*/
}

void TextSystem(hge_entity* entity, tag_component* ActiveCamera) {
  hge_mat4 projection_matrix;
  hgeUseShader(hgeResourcesQueryShader("text"));
  projection_matrix = hgeMathMat4OrthographicProjection(hgeWindowWidth(), hgeWindowHeight(), -100, 100);
  hgeShaderSetMatrix4(hgeResourcesQueryShader("text"), "projection", projection_matrix);
  hgeUseShader(hgeResourcesQueryShader("sprite_shader"));
}

int main() {
	hge_window window = { "River Fire", 1280, 720 };
	hgeInit(60, window, HGE_INIT_RENDERING | HGE_INIT_ECS);

	hgeResourcesLoadTexture("res/Palette_NTSC.png", "image");
	hgeResourcesLoadTexture("res/HGE/DEFAULT NORMAL.png", "DEFAULT NORMAL");

	hgeResourcesLoadMesh("res/tiles/flat.obj", "tile flat");
	hgeResourcesLoadMesh("res/tiles/top_left.obj", "tile topleft");
	hgeResourcesLoadMesh("res/tiles/top_right.obj", "tile topright");
	hgeResourcesLoadMesh("res/tiles/bottom_left.obj", "tile bottomleft");
	hgeResourcesLoadMesh("res/tiles/bottom_right.obj", "tile bottomright");
	hgeResourcesLoadMesh("res/tiles/bridge.obj", "tile bridge");

	hgeResourcesLoadMesh("res/tiles/dirt.obj", "dirt");
	hgeResourcesLoadMesh("res/tiles/dirt_topleft.obj", "dirt topleft");
	hgeResourcesLoadMesh("res/tiles/dirt_topright.obj", "dirt topright");
	hgeResourcesLoadMesh("res/tiles/dirt_bottomleft.obj", "dirt bottomleft");
	hgeResourcesLoadMesh("res/tiles/dirt_bottomright.obj", "dirt bottomright");
	hgeResourcesLoadMesh("res/water.obj", "water");
	hgeResourcesLoadMesh("res/tiles/fire/fire.obj", "fire");

	hgeResourcesLoadMesh("res/sphere map.obj", "sphere map");

	//hgeResourcesLoadMesh("res/tiles/grass.obj", "grass");
	hgeResourcesLoadMesh("res/tiles/bush.obj", "bush");
	hgeResourcesLoadMesh("res/pumkin.obj", "pumkin");
	hgeResourcesLoadMesh("res/tree.obj", "tree");
	hgeResourcesLoadMesh("res/tiles/tent.obj", "tent");
	hgeResourcesLoadMesh("res/tiles/table.obj", "table");

	hgeResourcesLoadMesh("res/propane.obj", "propane");

	hgeResourcesLoadMesh("res/firepit_rocks.obj", "firepit rocks");
	hgeResourcesLoadMesh("res/firepit.obj", "firepit logs");
	hgeResourcesLoadMesh("res/particle.obj", "particle");

	hgeResourcesLoadMesh("res/fire projectiles/fire proj 1.obj", "fire proj 1");

	hgeResourcesLoadMesh("res/molotov.obj", "molotov");

	hgeResourcesLoadShader("res/shaders/text.vs", NULL, "res/shaders/text.fs", "text");
	jamTextCreate("res/fonts/RifficFree-Bold.ttf");

	hgeAddBaseSystems();
	hgeAddSystem(TextSystem, 1, "ActiveCamera");
	hgeAddSystem(mesh_rendering_system, 2, "position", "mesh");
	hgeAddSystem(system_particles, 2, "position", "particle system");
	hgeAddSystem(system_level_selector, 3, "level selector", "Position", "Orientation");
	hgeAddSystem(system_player_controller, 3, "player controller", "Position", "Orientation");

	hgeAddSystem(System_CameraFX, 2, "Position", "camera fx");

	hgeAddSystem(system_level_manager, 1, "level manager");
	hgeAddSystem(system_bridge, 2, "position", "bridge");
	hgeAddSystem(system_projectile, 3, "position", "velocity", "projectile");
	hgeAddSystem(system_explosive, 2, "position", "explosive");


	hge_entity* level_manager_entity = hgeCreateEntity();
	level_manager level_manager_c = { 0.0f, 0.5f, 0.f };
	hgeAddComponent(level_manager_entity, hgeCreateComponent("level manager", &level_manager_c, sizeof(level_manager_c)));

	LoadLevel("res/levels/level selection.tmx");

	load_level_folder();
	hgeStart();
	clean_loaded_level_files();

	/*
	float some_value_write = 43.56f;
	float some_value_read = 0.0f;

	binwrite("res/levels/scores/river win.bin", &some_value_write, sizeof(some_value_write));
	binread("res/levels/scores/river win.bin", &some_value_read, sizeof(some_value_read));

	printf("read '%f'\n", some_value_read);
	*/
	return 0;
}
